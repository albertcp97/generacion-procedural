﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simpleVoxelFarm : MonoBehaviour {


	private GameObject currentBlockType;
	public GameObject[] blockTypes;
    public GameObject water;

	[Tooltip("True for minecraft style voxels")]
	public bool SnapToGrid = true;

    public int seed = 35002852;
	public float amp = 10f;
	public float freq = 10f;


	private Vector3 myPos;

	void Start () {
        //el mapa de perlin siempre devuelve la misma posicion.
        //si tu quieres que deveulva algo distinto tendras que hacer un desfase. Ese defase es conocido como "seed" (semilla)
        print(Mathf.PerlinNoise(5f, 2f));
		generateTerrain ();
	}
	


	void generateTerrain(){

        //determinas posicion inicial
		myPos = this.transform.position;

        //dices numero de filas y columnas
		int cols = 100;
		int rows = 100;


        //recorro el mapa como si fuera una matriz.
		for (int x = 0; x < cols; x++) {


			for (int z = 0; z < rows; z++) {
                //dejo una parte del mapa aislada para hacer una zona volcanica donde darte un baño de lava
                if (z > 30) { 
                float y = Mathf.PerlinNoise((myPos.x + x + seed) / freq,
                            (myPos.z + z) / freq) * amp;
                  //genero un perlin para hacer 3 biomas y distribuirlos por el mapa con sus caracteristicas
                float perlinbioma = Mathf.PerlinNoise((myPos.x + x + 5443) / freq,
                            (myPos.z + z) / freq);

                    //miramos el resultado del perlin y depende el resultado se le asignara un un bioma con sus caracteristicas
                    if (perlinbioma < 0.3f)
                    {

                        if (seed % 2 == 0)
                        {
                            print("wii sin");
                            y += Mathf.Sin(z) / 1000f;
                        }

                        if (SnapToGrid)
                        {
                            //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                            //y = Mathf.Floor(y);
                        }

                        
                        if (y > 3 * (amp / 4))
                        {
                            currentBlockType =
                          blockTypes[2];
                        }
                        



                        //si no hubiese el >=0, en los puntos que se queda justo a 0 habria un agujero
                        for (float i = y; i >= y - 5; i--)
                        {
                            GameObject newBlock =
                          GameObject.Instantiate(currentBlockType);

                            newBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }

                        for (float i = y; i >= y; i--)
                        {
                            if (y > 3 * (amp / 4))
                            {
                                currentBlockType =
                              blockTypes[2];
                            }
                            else
                            {   //esto genera de manera random arboles en la superficie, sino dejara bloque base de ese bioma (Alvaro help me)
                                if (Random.Range(0, 100) == 1)
                                {

                                    GameObject waterBlock =
                                  GameObject.Instantiate(blockTypes[4]);

                                    waterBlock.transform.position =
                                        new Vector3(myPos.x + x, i, myPos.z + z);
                                }
                                else
                                {
                                    GameObject waterBlock =
                                  GameObject.Instantiate(blockTypes[3]);

                                    waterBlock.transform.position =
                                        new Vector3(myPos.x + x, i, myPos.z + z);
                                }
                            }

                        }


                        
                    }
                    else if (perlinbioma < 0.6f)

                    {



                        if (seed % 9 == 0)
                        {
                            print("wii sin");
                            y += Mathf.Sin(z) / 2.3f;
                        }

                        if (SnapToGrid)
                        {
                            //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                            //y = Mathf.Floor(y);
                        }

                        if (y > 3 * (amp / 4))
                            currentBlockType =
                            blockTypes[2];
                        else if (y > 2 * (amp / 5))
                            currentBlockType = blockTypes[1];
                        else
                            currentBlockType =
                            blockTypes[0];

                        //si no hubiese el >=0, en los puntos que se queda justo a 0 habria un agujero
                        for (float i = y; i >= y - 5; i--)
                        {
                            GameObject newBlock =
                          GameObject.Instantiate(currentBlockType);

                            newBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }
                        for (float i = y; i < (amp / 5); i++)
                        {
                            GameObject waterBlock =
                          GameObject.Instantiate(water);

                            waterBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }
                    }
                    else
                    {
                        if (seed % 9 == 0)
                        {
                            print("wii sin");
                            y += Mathf.Sin(z) / 1000f;
                        }

                        if (SnapToGrid)
                        {
                            //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                            //y = Mathf.Floor(y);
                        }


                        currentBlockType =
                        blockTypes[7];

                        //si no hubiese el >=0, en los puntos que se queda justo a 0 habria un agujero
                        for (float i = y; i >= y - 5; i--)
                        {
                            GameObject newBlock =
                          GameObject.Instantiate(currentBlockType);

                            newBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }
                        for (float i = y; i < (amp / 5); i++)
                        {
                            GameObject waterBlock =
                          GameObject.Instantiate(water);

                            waterBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }
                    }

                }
                else 
                {
                    //esta parte generara en una parte del mapa una zona volcanica con lava funciona igual que las otra, pero el nivel de la lava es superior
                    float y = Mathf.PerlinNoise((myPos.x + x + seed) / freq,
                               (myPos.z + z) / freq) * amp;
                    float perlinbioma = Mathf.PerlinNoise((myPos.x + x + 5443) / (freq-8),
                            (myPos.z + z) / (freq-8))*5;

                   

                        if (seed % 10 == 0)
                        {
                            print("wii sin");
                            y += Mathf.Sin(z) / 1000f;
                        }

                        if (SnapToGrid)
                        {
                            //fuerzas a que la y sea un entero (las diferencias seran a nivel bloque, como si fuera minecraft)
                            //y = Mathf.Floor(y);
                        }

                       
                            currentBlockType =
                          blockTypes[6];
                       


                        //si no hubiese el >=0, en los puntos que se queda justo a 0 habria un agujero
                        for (float i = y; i >= y - 5; i--)
                        {
                            GameObject newBlock =
                          GameObject.Instantiate(currentBlockType);

                            newBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }

                        

                        for (float i = y; i < (amp / 2); i++)
                        {
                            GameObject waterBlock =
                          GameObject.Instantiate(blockTypes[5]);

                            waterBlock.transform.position =
                                new Vector3(myPos.x + x, i, myPos.z + z);
                        }

                }

            }
                    //si la x,z son muy pequeñas al dividirlas por una freq grande se van a quedar a 0 y luego se multiplicaran por una amplitud grande.
                    //perlinnoise va a devolver la intensidad del mapa perlin de la posicion
                  

                

               




            }

        }

      

    }



